package org.example.krit.kban;

import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;
import org.example.krit.kban.commands.CommandManager;
import org.example.krit.kban.listeners.CommandSendListener;
import org.example.krit.kban.listeners.InventoryClickListener;
import org.example.krit.kban.listeners.PlayerLoginListener;
import org.example.krit.kban.listeners.PlayerMessageSendListener;
import org.example.krit.kban.menusystem.PlayerMenuUtility;
import org.example.krit.kban.utils.*;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.logging.Filter;

public final class KBan extends JavaPlugin {

    private static KBan instance;

    private static final HashMap<Player, PlayerMenuUtility> playerMenuUtilityMap = new HashMap<>();

    @Override
    public void onEnable() {
        instance = this;

        // Команды
        getCommand("kban").setExecutor(new CommandManager());

        // Конфиг
        saveDefaultConfig();

        // Прослушиватели событий
        getServer().getPluginManager().registerEvents(new PlayerLoginListener(), this);
        getServer().getPluginManager().registerEvents(new InventoryClickListener(), this);
        getServer().getPluginManager().registerEvents(new CommandSendListener(), this);
        getServer().getPluginManager().registerEvents(new PlayerMessageSendListener(), this);

        // Загрузить банлист из файла JSON
        try {
            BanListUtils.loadBanList();
        } catch (IOException e) {
            e.printStackTrace();
        }
        // Загрузить варнлист из файла JSON
        try {
            WarnsUtils.loadWarnsList();
        } catch (IOException e) {
            e.printStackTrace();
        }
        // Загрузить адреслист из файла JSON
        try {
            PlayerAddressUtils.loadAddresses();
        } catch (IOException e) {
            e.printStackTrace();
        }
        // Загрузить мутлист из файла JSON
        try {
            MuteUtils.loadMutesList();
        } catch (IOException e) {
            e.printStackTrace();
        }

        KBanUtils.convertMojangBansToKBan();
    }

    public static PlayerMenuUtility getPlayerMenuUtility(Player p) {
        PlayerMenuUtility playerMenuUtility;
        if (!(playerMenuUtilityMap.containsKey(p))) { //See if the player has a playermenuutility "saved" for them

            //This player doesn't. Make one for them add add it to the hashmap
            playerMenuUtility = new PlayerMenuUtility(p);
            playerMenuUtilityMap.put(p, playerMenuUtility);

            return playerMenuUtility;
        } else {
            return playerMenuUtilityMap.get(p); //Return the object by using the provided player
        }
    }



    public static KBan getInstance() {
        return instance;
    }
}

package org.example.krit.kban.commands;

import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.command.TabCompleter;
import org.bukkit.util.StringUtil;
import org.example.krit.kban.commands.subcommands.*;
import org.example.krit.kban.model.Mute;
import org.example.krit.kban.utils.KBanUtils;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.ArrayList;
import java.util.List;

public class CommandManager implements CommandExecutor, TabCompleter {

    private ArrayList<SubCommand> subcommands = new ArrayList<>();

    public CommandManager(){
        subcommands.add(new ReloadCommand());
        subcommands.add(new BanCommand());
        subcommands.add(new UnbanCommand());
        subcommands.add(new BanListCommand());
        subcommands.add(new IpBanCommand());
        subcommands.add(new WarnCommand());
        subcommands.add(new CheckCommand());
        subcommands.add(new KickCommand());
        subcommands.add(new MuteCommand());
        subcommands.add(new WarnListCommand());
        subcommands.add(new UnmuteCommand());
        subcommands.add(new MuteListCommand());
    }

    @Override
    public boolean onCommand(@NotNull CommandSender sender, @NotNull Command command, @NotNull String label, String[] args) {


            if (args.length > 0){
                for (int i = 0; i < getSubcommands().size(); i++){
                    if (args[0].equalsIgnoreCase(getSubcommands().get(i).getName())){
                        getSubcommands().get(i).perform(sender, args);
                    }
                }
            } else {
                KBanUtils.send(sender, ChatColor.translateAlternateColorCodes('&', "&6KBan &r-&c Список команд"));
                for (int i = 0; i < getSubcommands().size(); i++){
                    KBanUtils.send(sender, ChatColor.YELLOW + getSubcommands().get(i).getSyntax() + ChatColor.WHITE + " - " + getSubcommands().get(i).getDescription());
                }
                KBanUtils.send(sender, ChatColor.DARK_GRAY + "--------------------------------");
            }




        return true;
    }

    public ArrayList<SubCommand> getSubcommands(){
        return subcommands;
    }

    @Override
    public @Nullable List<String> onTabComplete(@NotNull CommandSender sender, @NotNull Command command, @NotNull String s, @NotNull String[] args) {

        if (args.length == 1) {

            // Возвращает все команды плагина
            List<String> strings = new ArrayList<>();

            for (SubCommand subCommand : subcommands) {
                strings.add(subCommand.getName());
            }

            List<String> completions = new ArrayList<>();
            StringUtil.copyPartialMatches(args[0], strings, completions);
            return completions;

        } else if (args.length >= 2) {

            // Для остальных команд
            for (SubCommand subCommand : subcommands) {
                if (subCommand.getName().equalsIgnoreCase(args[0])) {
                    return subCommand.getCompleters(args);
                }
            }
        }

        return null;
    }
}

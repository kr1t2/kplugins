package org.example.krit.kban.commands.subcommands;

import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.example.krit.kban.KBan;
import org.example.krit.kban.commands.SubCommand;
import org.example.krit.kban.menusystem.PlayerMenuUtility;
import org.example.krit.kban.menusystem.menu.Banlist;

import java.util.List;
import java.util.Objects;

public class BanListCommand extends SubCommand {
    @Override
    public String getName() {
        return "banlist";
    }

    @Override
    public String getDescription() {
        return "Открыть список забаненных игроков";
    }

    @Override
    public String getSyntax() {
        return "/kban banlist";
    }

    @Override
    public void perform(CommandSender sender, String[] args) {
        if (sender instanceof Player p) {
            if (!p.hasPermission("kban.banlist")) {
                p.sendMessage(ChatColor.translateAlternateColorCodes('&', Objects.requireNonNull(KBan.getInstance().getConfig().getString("no-permission-message"))));
                return;
            }
            new Banlist(new PlayerMenuUtility(p)).open();
        }
    }

    @Override
    public List<String> getCompleters(String[] args) {
        return null;
    }
}

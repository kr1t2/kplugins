package org.example.krit.kban.commands.subcommands;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.OfflinePlayer;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.util.StringUtil;
import org.example.krit.kban.KBan;
import org.example.krit.kban.commands.SubCommand;
import org.example.krit.kban.menusystem.PlayerMenuUtility;
import org.example.krit.kban.menusystem.menu.BannedPlayerManager;
import org.example.krit.kban.utils.KBanUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class CheckCommand extends SubCommand {
    @Override
    public String getName() {
        return "check";
    }

    @Override
    public String getDescription() {
        return "Получить информацию об игроке: предупреждениях, в бане или нет, IP адрес, UUID";
    }

    @Override
    public String getSyntax() {
        return "/kban check <player>";
    }

    @Override
    public void perform(CommandSender sender, String[] args) {

        if (args.length == 1) {
            KBanUtils.send(sender, ChatColor.RED + "Использование команды: " + ChatColor.YELLOW + getSyntax());
            return;
        }

        if (sender instanceof Player p) {
            if (!p.hasPermission("kban.check")) {
                p.sendMessage(ChatColor.translateAlternateColorCodes('&', Objects.requireNonNull(KBan.getInstance().getConfig().getString("no-permission-message"))));
                return;
            }
            OfflinePlayer target = Bukkit.getOfflinePlayer(args[1]);
            if (!target.hasPlayedBefore()) {
                return;
            }
            new BannedPlayerManager(new PlayerMenuUtility(p, target)).open();
        }
    }

    @Override
    public List<String> getCompleters(String[] args) {

        if (args.length == 2) {
            ArrayList<String> playerNames = new ArrayList<>();
            for (OfflinePlayer player : Bukkit.getOfflinePlayers()) {
                playerNames.add(player.getName());
            }
            List<String> completions = new ArrayList<>();
            StringUtil.copyPartialMatches(args[1], playerNames, completions);
            return completions;
        }
        return null;
    }
}

package org.example.krit.kban.commands.subcommands;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.BlockCommandSender;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.util.StringUtil;
import org.example.krit.kban.KBan;
import org.example.krit.kban.commands.SubCommand;
import org.example.krit.kban.utils.KBanUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class KickCommand extends SubCommand {
    @Override
    public String getName() {
        return "kick";
    }

    @Override
    public String getDescription() {
        return "Кикает игрока с сервера";
    }

    @Override
    public String getSyntax() {
        return "/kick <player> <reason>";
    }

    @Override
    public void perform(CommandSender sender, String[] args) {
        if (sender instanceof Player p && !p.hasPermission("kban.kick")) {
            p.sendMessage(ChatColor.translateAlternateColorCodes('&', Objects.requireNonNull(KBan.getInstance().getConfig().getString("no-permission-message"))));
            return;
        } else if (sender instanceof BlockCommandSender) {
            return;
        }

        if (args.length == 1) {
            KBanUtils.send(sender, ChatColor.RED + "Использование команды: " + ChatColor.YELLOW + getSyntax());
            return;
        }

        // 2. Получаем игрока которого нужно кикнуть
        Player target = Bukkit.getPlayerExact(args[1]);
        // Проверям, существует ли игрок
        if (target == null) {
            KBanUtils.send(sender, ChatColor.translateAlternateColorCodes('&', Objects.requireNonNull(KBan.getInstance().getConfig().getString("player-doesn't-exist"))));
            return;
        }

        // 3. Получаем причину кика
        StringBuilder reasonBuilder = new StringBuilder();
        for (int i = 2; i < args.length; i++) {
            reasonBuilder.append(args[i]).append(" ");
        }
        String reason = reasonBuilder.toString();

        if (reason.isEmpty() && KBan.getInstance().getConfig().getBoolean("only-with-reason.kick")) {
            KBanUtils.send(sender, ChatColor.translateAlternateColorCodes('&', Objects.requireNonNull(KBan.getInstance().getConfig().getString("need-reason-message"))));
            return;
        } else if (reason.isEmpty()) {
            reason = "Причина не указана";
        }

        //Кик игрока

        KBanUtils.kickPlayer(target, sender.getName(), reason);
    }

    @Override
    public List<String> getCompleters(String[] args) {
        if (args.length == 2) {
            ArrayList<String> playerNames = new ArrayList<>();
            for (Player player : Bukkit.getOnlinePlayers()) {
                playerNames.add(player.getName());
            }
            List<String> completions = new ArrayList<>();
            StringUtil.copyPartialMatches(args[1], playerNames, completions);
            return completions;
        } else if (args.length > 2) {
            return List.of("Пишите здесь причину кика");
        }
        return null;
    }
}

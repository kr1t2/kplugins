package org.example.krit.kban.commands.subcommands;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.OfflinePlayer;
import org.bukkit.command.BlockCommandSender;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.util.StringUtil;
import org.example.krit.kban.KBan;
import org.example.krit.kban.commands.SubCommand;
import org.example.krit.kban.utils.KBanUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class MuteCommand extends SubCommand {
    @Override
    public String getName() {
        return "mute";
    }

    @Override
    public String getDescription() {
        return "Заставит игрока ничего не говорить";
    }

    @Override
    public String getSyntax() {
        return "/kban mute <player> <time: time> <reason: reason>";
    }

    @Override
    public void perform(CommandSender sender, String[] args) {
        if (sender instanceof Player p && !p.hasPermission("kban.mute")) {
            p.sendMessage(ChatColor.translateAlternateColorCodes('&', Objects.requireNonNull(KBan.getInstance().getConfig().getString("no-permission-message"))));
            return;
        } else if (sender instanceof BlockCommandSender) {
            return;
        }

        if (args.length == 1) {
            KBanUtils.send(sender, ChatColor.RED + "Использование команды: " + ChatColor.YELLOW + getSyntax());
            return;
        }

        // 2. Получаем игрока которого нужно замьютить
        OfflinePlayer target = Bukkit.getOfflinePlayer(args[1]);
        // Проверям, существует ли игрок
        if (!target.hasPlayedBefore()) {
            KBanUtils.send(sender, ChatColor.translateAlternateColorCodes('&', Objects.requireNonNull(KBan.getInstance().getConfig().getString("player-doesn't-exist"))));
            return;
        }

        // 2. Берём аргументы с команды: время бана и причину
        String argNow = "0";
        long days = 0;
        long hours = 0;
        long minutes = 0;
        long seconds = 0;

        StringBuilder reasonBuilder = new StringBuilder();
        for (String arg : args) {
            if (arg.equalsIgnoreCase("time:")) {
                argNow = arg;
            } else if (arg.equalsIgnoreCase("reason:")) {
                argNow = arg;
            } else if (argNow.equalsIgnoreCase("time:")) {
                if (arg.charAt(arg.length() - 1) == 'd') {
                    days = Integer.parseInt(arg.substring(0, arg.length() - 1));
                } else if (arg.charAt(arg.length() - 1) == 'h') {
                    hours = Integer.parseInt(arg.substring(0, arg.length() - 1));
                } else if (arg.charAt(arg.length() - 1) == 'm') {
                    minutes = Integer.parseInt(arg.substring(0, arg.length() - 1));
                } else if (arg.charAt(arg.length() - 1) == 's') {
                    seconds = Integer.parseInt(arg.substring(0, arg.length() - 1));
                }
            } else if (argNow.equalsIgnoreCase("reason:")) {
                reasonBuilder.append(arg).append(" ");
            }
        }
        String reason = reasonBuilder.toString();

        if (reason.isEmpty() && KBan.getInstance().getConfig().getBoolean("only-with-reason.mute")) {
            KBanUtils.send(sender, ChatColor.translateAlternateColorCodes('&', Objects.requireNonNull(KBan.getInstance().getConfig().getString("need-reason-message"))));
            return;
        } else if (reason.isEmpty()) {
            reason = "Причина не указана";
        }

        // Конвертируем время в миллисекунды и часы для удобного просмотра
        long totalInMs = days * 86400000 + hours * 3600000 + minutes * 60000 + seconds * 1000;

        //Мут игрока
        KBanUtils.mute(target, sender instanceof Player p ? p.getUniqueId().toString() : "console", reason, totalInMs == 0 ? 0 : totalInMs + System.currentTimeMillis());
    }

    @Override
    public List<String> getCompleters(String[] args) {
        if (args.length == 2) {
            ArrayList<String> playerNames = new ArrayList<>();
            for (OfflinePlayer player : Bukkit.getOfflinePlayers()) {
                playerNames.add(player.getName());
            }
            List<String> completions = new ArrayList<>();
            StringUtil.copyPartialMatches(args[1], playerNames, completions);
            return completions;
        } else if (args.length > 2) {
            String argNow = "0";
            for (int i = args.length - 1; i < args.length; i--) {
                if (args[i].equalsIgnoreCase("time:") || args[i].equalsIgnoreCase("reason:")) {
                    argNow = args[i];
                    break;
                }
                if (i == 0) {
                    break;
                }
            }
            if (argNow.equalsIgnoreCase("time:")) {
                return List.of("1d", "1h", "1m", "1s", "reason:");
            } else if (argNow.equalsIgnoreCase("reason:")) {
                return List.of("Пишите здесь причину мута", "time:");
            } else {
                return List.of("time:", "reason:");
            }
        }
        return null;
    }
}

package org.example.krit.kban.commands.subcommands;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.BlockCommandSender;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.example.krit.kban.KBan;
import org.example.krit.kban.commands.SubCommand;

import java.util.ArrayList;
import java.util.Objects;

public class ReloadCommand extends SubCommand {
    @Override
    public String getName() {
        return "reload";
    }

    @Override
    public String getDescription() {
        return "Перезагружает плагин и его конфигурацию";
    }

    @Override
    public String getSyntax() {
        return "/kban reload";
    }

    @Override
    public void perform(CommandSender sender, String[] args) {
        if (sender instanceof Player p) {
            if (!p.hasPermission("kban.reload")) {
                p.sendMessage(ChatColor.translateAlternateColorCodes('&', Objects.requireNonNull(KBan.getInstance().getConfig().getString("no-permission-message"))));

            } else {
                KBan.getInstance().reloadConfig();
                p.sendMessage(ChatColor.translateAlternateColorCodes('&', Objects.requireNonNull(KBan.getInstance().getConfig().getString("reload-message"))));
            }
            return;
        } else if (sender instanceof BlockCommandSender) {
            return;
        }
        KBan.getInstance().reloadConfig();;
        Bukkit.getConsoleSender().sendMessage(ChatColor.translateAlternateColorCodes('&', Objects.requireNonNull(KBan.getInstance().getConfig().getString("reload-message"))));

    }

    @Override
    public ArrayList<String> getCompleters(String[] args) {
        return null;
    }
}

package org.example.krit.kban.commands.subcommands;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.OfflinePlayer;
import org.bukkit.command.BlockCommandSender;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.util.StringUtil;
import org.example.krit.kban.KBan;
import org.example.krit.kban.commands.SubCommand;
import org.example.krit.kban.model.Ban;
import org.example.krit.kban.utils.BanListUtils;
import org.example.krit.kban.utils.KBanUtils;
import org.example.krit.kban.utils.MessagesUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.UUID;

public class UnbanCommand extends SubCommand {
    @Override
    public String getName() {
        return "unban";
    }

    @Override
    public String getDescription() {
        return "Разбанивает игрока";
    }

    @Override
    public String getSyntax() {
        return "/kban unban <player>";
    }

    @Override
    public void perform(CommandSender sender, String[] args) {
        // 1. Когда у игрока нет разрешения на разбан
        if (sender instanceof Player p && !p.hasPermission("kban.unban")) {
            p.sendMessage(ChatColor.translateAlternateColorCodes('&', Objects.requireNonNull(KBan.getInstance().getConfig().getString("no-permission-message"))));
            return;
        } else if (sender instanceof BlockCommandSender) {
            return;
        }

        if (args.length == 1) {
            KBanUtils.send(sender, ChatColor.RED + "Использование команды: " + ChatColor.YELLOW + getSyntax());
            return;
        }

        // 2. Получаем игрока которого нужно разбанить
        OfflinePlayer target = Bukkit.getOfflinePlayer(args[1]);
        // Проверям, существует ли игрок
        if (!target.hasPlayedBefore()) {
            KBanUtils.send(sender, ChatColor.translateAlternateColorCodes('&', Objects.requireNonNull(KBan.getInstance().getConfig().getString("player-doesn't-exist"))));
            return;
        }
        if (BanListUtils.findBan(target.getUniqueId().toString()) == null) {
            KBanUtils.send(sender, ChatColor.translateAlternateColorCodes('&', Objects.requireNonNull(KBan.getInstance().getConfig().getString("player-not-banned-message")
                    .replaceAll("<player>", Objects.requireNonNull(target.getName()))
            )));
            return;
        }

        KBanUtils.unban(target.getUniqueId().toString());

        // 4. Отправляем сообщение об успешном разбане
        if (KBan.getInstance().getConfig().getBoolean("send-to-everyone.bans")) {
            MessagesUtils.sendUnbanMessage(target);
        } else {
            MessagesUtils.sendUnbanMessage(sender, target);
        }

    }

    @Override
    public List<String> getCompleters(String[] args) {

        if (args.length == 2) {
            ArrayList<String> bannedPlayers = new ArrayList<>();
            List<Ban> banList = BanListUtils.getBanList();
            for (int i = 0; i < banList.size(); i++) {
                if (System.currentTimeMillis() > banList.get(i).getBanExpires() && banList.get(i).getBanExpires() != 0) {
                    KBanUtils.unban(banList.get(i).getUuid());
                    i--;
                    continue;
                }
                OfflinePlayer p = Bukkit.getOfflinePlayer(UUID.fromString(banList.get(i).getUuid()));
                bannedPlayers.add(p.getName());
            }
            List<String> completions = new ArrayList<>();
            StringUtil.copyPartialMatches(args[1], bannedPlayers, completions);
            return completions;
        }

        return null;
    }
}

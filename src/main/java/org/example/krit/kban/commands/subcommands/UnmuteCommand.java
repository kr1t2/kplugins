package org.example.krit.kban.commands.subcommands;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.OfflinePlayer;
import org.bukkit.command.BlockCommandSender;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.util.StringUtil;
import org.example.krit.kban.KBan;
import org.example.krit.kban.commands.SubCommand;
import org.example.krit.kban.model.Ban;
import org.example.krit.kban.model.Mute;
import org.example.krit.kban.utils.BanListUtils;
import org.example.krit.kban.utils.KBanUtils;
import org.example.krit.kban.utils.MuteUtils;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.UUID;

public class UnmuteCommand extends SubCommand {
    @Override
    public String getName() {
        return "unmute";
    }

    @Override
    public String getDescription() {
        return "Размучивает игрока";
    }

    @Override
    public String getSyntax() {
        return "/kban unmute <player>";
    }

    @Override
    public void perform(CommandSender sender, String[] args) {
        // 1. Когда у игрока нет разрешения на размут
        if (sender instanceof Player p && !p.hasPermission("kban.unmute")) {
            p.sendMessage(ChatColor.translateAlternateColorCodes('&', Objects.requireNonNull(KBan.getInstance().getConfig().getString("no-permission-message"))));
            return;
        } else if (sender instanceof BlockCommandSender) {
            return;
        }

        if (args.length == 1) {
            KBanUtils.send(sender, ChatColor.RED + "Использование команды: " + ChatColor.YELLOW + getSyntax());
            return;
        }

        // 2. Получаем игрока которого нужно размутить
        OfflinePlayer target = Bukkit.getOfflinePlayer(args[1]);
        // Проверям, существует ли игрок
        if (!target.hasPlayedBefore()) {
            KBanUtils.send(sender, ChatColor.translateAlternateColorCodes('&', Objects.requireNonNull(KBan.getInstance().getConfig().getString("player-doesn't-exist"))));
            return;
        }
        if (MuteUtils.findMute(target.getUniqueId().toString()) == null) {
            KBanUtils.send(sender, ChatColor.translateAlternateColorCodes('&', Objects.requireNonNull(KBan.getInstance().getConfig().getString("player-not-muted-message")
                    .replaceAll("<player>", Objects.requireNonNull(target.getName()))
            )));
            return;
        }

        try {
            MuteUtils.deleteMute(target.getUniqueId().toString());
        } catch (IOException e) {
            throw new RuntimeException(e);
        }

        KBanUtils.send(sender, ChatColor.translateAlternateColorCodes('&', Objects.requireNonNull(KBan.getInstance().getConfig().getString("player-unmute-log-message"))
                .replaceAll("<player>", Objects.requireNonNull(target.getName()))
        ));
        if (target.isOnline()) {
            ((Player) target).sendMessage(ChatColor.translateAlternateColorCodes('&', Objects.requireNonNull(KBan.getInstance().getConfig().getString("player-unmute-message"))
                    .replaceAll("<player>", sender instanceof Player p ? p.getName() : "console")
            ));
        }
    }

    @Override
    public List<String> getCompleters(String[] args) {
        if (args.length == 2) {
            ArrayList<String> mutedPlayer = new ArrayList<>();
            List<Mute> mutesList = MuteUtils.getMutesList();
            for (int i = 0; i < mutesList.size(); i++) {
                if (System.currentTimeMillis() > mutesList.get(i).getExpiresAt() && mutesList.get(i).getExpiresAt() != 0) {
                    KBanUtils.unban(mutesList.get(i).getUuid());
                    i--;
                    continue;
                }
                OfflinePlayer p = Bukkit.getOfflinePlayer(UUID.fromString(mutesList.get(i).getUuid()));
                mutedPlayer.add(p.getName());
            }
            List<String> completions = new ArrayList<>();
            StringUtil.copyPartialMatches(args[1], mutedPlayer, completions);
            return completions;
        }
        return null;
    }
}

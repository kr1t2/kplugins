package org.example.krit.kban.commands.subcommands;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.OfflinePlayer;
import org.bukkit.command.BlockCommandSender;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.util.StringUtil;
import org.example.krit.kban.KBan;
import org.example.krit.kban.commands.SubCommand;
import org.example.krit.kban.model.Warn;
import org.example.krit.kban.model.Warns;
import org.example.krit.kban.utils.KBanUtils;
import org.example.krit.kban.utils.MessagesUtils;
import org.example.krit.kban.utils.WarnsUtils;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class WarnCommand extends SubCommand {
    @Override
    public String getName() {
        return "warn";
    }

    @Override
    public String getDescription() {
        return "Выдаёт игроку предупреждение";
    }

    @Override
    public String getSyntax() {
        return "/kban warn <player> <reason>";
    }

    @Override
    public void perform(CommandSender sender, String[] args) {
        if (sender instanceof Player p && !p.hasPermission("kban.warn")) {
            p.sendMessage(ChatColor.translateAlternateColorCodes('&', Objects.requireNonNull(KBan.getInstance().getConfig().getString("no-permission-message"))));
            return;
        } else if (sender instanceof BlockCommandSender) {
            return;
        }

        if (args.length == 1) {
            KBanUtils.send(sender, ChatColor.RED + "Использование команды: " + ChatColor.YELLOW + getSyntax());
            return;
        }

        // 2. Получаем игрока которого нужно предупредить
        OfflinePlayer target = Bukkit.getOfflinePlayer(args[1]);
        // Проверям, существует ли игрок
        if (!target.hasPlayedBefore()) {
            KBanUtils.send(sender, ChatColor.translateAlternateColorCodes('&', Objects.requireNonNull(KBan.getInstance().getConfig().getString("player-doesn't-exist"))));
            return;
        }

        // 3. Получаем причину варна
        StringBuilder reasonBuilder = new StringBuilder();
        for (int i = 2; i < args.length; i++) {
            reasonBuilder.append(args[i]).append(" ");
        }
        String reason = reasonBuilder.toString();

        if (reason.isEmpty() && KBan.getInstance().getConfig().getBoolean("only-with-reason.warn")) {
            KBanUtils.send(sender, ChatColor.translateAlternateColorCodes('&', Objects.requireNonNull(KBan.getInstance().getConfig().getString("need-reason-message"))));
            return;
        } else if (reason.isEmpty()) {
            reason = "Причина не указана";
        }

        // Выдаём игроку предупреждение
        ArrayList<Warn> warns = new ArrayList<>();
        if (WarnsUtils.findWarns(target.getUniqueId().toString()) != null) {
            warns = WarnsUtils.findWarns(target.getUniqueId().toString()).getWarns();
        } else {
            WarnsUtils.addWarns(target.getUniqueId().toString(), warns);
        }
        Warn warn = new Warn(sender instanceof Player p ? p.getUniqueId().toString() : "console", reason, System.currentTimeMillis(), warns.size() + 1);
        warns.add(warn);

        try {
            WarnsUtils.updateWarns(target.getUniqueId().toString(), new Warns(target.getUniqueId().toString(), warns));
        } catch (IOException e) {
            throw new RuntimeException(e);
        }

        KBanUtils.checkWarns(target.getUniqueId().toString(), sender);

        // Пишем игроку что он выдал предупреждение
        if (KBan.getInstance().getConfig().getBoolean("send-to-everyone.bans")) {
            MessagesUtils.sendWarnMessage(warn, target);
        } else {
            MessagesUtils.sendWarnMessage(warn, target, sender);
        }
    }

    @Override
    public List<String> getCompleters(String[] args) {
        if (args.length == 2) {
            ArrayList<String> playerNames = new ArrayList<>();
            for (OfflinePlayer player : Bukkit.getOfflinePlayers()) {
                playerNames.add(player.getName());
            }
            List<String> completions = new ArrayList<>();
            StringUtil.copyPartialMatches(args[1], playerNames, completions);
            return completions;
        } else if (args.length > 2) {
            return List.of("Пишите здесь причину предупреждения");
        }
        return null;
    }
}

package org.example.krit.kban.commands.subcommands;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.OfflinePlayer;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.util.StringUtil;
import org.example.krit.kban.KBan;
import org.example.krit.kban.commands.SubCommand;
import org.example.krit.kban.menusystem.PlayerMenuUtility;
import org.example.krit.kban.menusystem.menu.Banlist;
import org.example.krit.kban.menusystem.menu.PlayerWarnsList;
import org.example.krit.kban.utils.KBanUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class WarnListCommand extends SubCommand {
    @Override
    public String getName() {
        return "getWarns";
    }

    @Override
    public String getDescription() {
        return "Получить предупреждения какого-либо игрока. Оставьте пустым первый параметр чтобы посмотреть свои предупреждения";
    }

    @Override
    public String getSyntax() {
        return "/kban getWarns <player>";
    }

    @Override
    public void perform(CommandSender sender, String[] args) {
        if (sender instanceof Player p) {
            if (args.length == 1) {
                if (!p.hasPermission("kban.mywarnlist")) {
                    p.sendMessage(ChatColor.translateAlternateColorCodes('&', Objects.requireNonNull(KBan.getInstance().getConfig().getString("no-permission-message"))));
                } else {
                    new PlayerWarnsList(new PlayerMenuUtility(p, p)).open();
                }
            } else if (args.length > 1) {
                if (!p.hasPermission("kban.warnlist")) {
                    p.sendMessage(ChatColor.translateAlternateColorCodes('&', Objects.requireNonNull(KBan.getInstance().getConfig().getString("no-permission-message"))));
                } else {
                    OfflinePlayer target = Bukkit.getOfflinePlayer(args[1]);
                    if (!target.hasPlayedBefore()) {
                        KBanUtils.send(sender, ChatColor.translateAlternateColorCodes('&', Objects.requireNonNull(KBan.getInstance().getConfig().getString("player-doesn't-exist"))));
                        return;
                    }
                    new PlayerWarnsList(new PlayerMenuUtility(p, target)).open();
                }
            }
        }
    }

    @Override
    public List<String> getCompleters(String[] args) {
        if (args.length == 2) {
            ArrayList<String> playerNames = new ArrayList<>();
            for (OfflinePlayer player : Bukkit.getOfflinePlayers()) {
                playerNames.add(player.getName());
            }
            List<String> completions = new ArrayList<>();
            StringUtil.copyPartialMatches(args[1], playerNames, completions);
            return completions;
        }
        return null;
    }
}

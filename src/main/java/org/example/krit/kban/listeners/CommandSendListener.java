package org.example.krit.kban.listeners;

import org.bukkit.ChatColor;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerCommandPreprocessEvent;
import org.example.krit.kban.KBan;

import java.util.Objects;

public class CommandSendListener implements Listener {

    @EventHandler
    public void onCommandSend(PlayerCommandPreprocessEvent e) {
        if((e.getMessage().contains("/ban") || e.getMessage().contains("/pardon") || e.getMessage().contains("/ban-ip")) && !KBan.getInstance().getConfig().getBoolean("allow-mojang-ban")) {
            e.getPlayer().sendMessage(ChatColor.translateAlternateColorCodes('&', Objects.requireNonNull(KBan.getInstance().getConfig().getString("use-other-command-message"))));
            e.setCancelled(true);
        }
    }
}

package org.example.krit.kban.listeners;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerLoginEvent;
import org.example.krit.kban.model.Ban;
import org.example.krit.kban.model.PlayerAddress;
import org.example.krit.kban.utils.BanListUtils;
import org.example.krit.kban.utils.KBanUtils;
import org.example.krit.kban.utils.PlayerAddressUtils;

import java.io.IOException;
import java.util.Objects;

public class PlayerLoginListener implements Listener {

    @EventHandler
    public void onPlayerLogin(PlayerLoginEvent event) throws IOException {

        Player p = event.getPlayer();

        // Записать его IP в JSON для дальнейшего использования
        PlayerAddress playerAddress = PlayerAddressUtils.findAddress(p.getUniqueId().toString());
        if (playerAddress == null) {
            PlayerAddressUtils.addAddress(p.getUniqueId().toString(), event.getAddress().getHostAddress());
        } else {
            playerAddress.setAddress(Objects.requireNonNull(event.getAddress().getHostAddress()));
            PlayerAddressUtils.updateAddress(p.getUniqueId().toString(), playerAddress);
        }

        // если IP Адрес игрока забанен не пускать на сервер
        for (int i = 0; i < BanListUtils.getBanList().size(); i++) {
            if (BanListUtils.getBanList().get(i).getIp() == null) continue;
            if (BanListUtils.getBanList().get(i).getBanExpires() < System.currentTimeMillis() && BanListUtils.getBanList().get(i).getBanExpires() != 0) {
                BanListUtils.unban(BanListUtils.getBanList().get(i).getUuid());
                i--;
                continue;
            }
            if (BanListUtils.getBanList().get(i).getIp().equals(event.getAddress().getHostAddress())) {
                event.disallow(PlayerLoginEvent.Result.KICK_BANNED, KBanUtils.getBanMessageComponent(BanListUtils.getBanList().get(i), event.getPlayer().getName()));
                return;
            }
        }

        Ban ban = BanListUtils.findBan(p.getUniqueId().toString());
        if (ban != null) {
            if (ban.isIpBan()) {
                if (ban.getIp() == null) {
                    ban.setIp(event.getAddress().getHostAddress());
                    BanListUtils.updateBan(p.getUniqueId().toString(), ban);
                }
                if (System.currentTimeMillis() > ban.getBanExpires() && ban.getBanExpires() != 0) {
                    KBanUtils.unban(p.getUniqueId().toString());
                    event.allow();
                } else {

                    event.disallow(PlayerLoginEvent.Result.KICK_BANNED, KBanUtils.getBanMessageComponent(ban, event.getPlayer().getName()));
                }
                return;
            }
            if (System.currentTimeMillis() > ban.getBanExpires() && ban.getBanExpires() != 0) {
                KBanUtils.unban(p.getUniqueId().toString());
                event.allow();
            } else {
                event.disallow(PlayerLoginEvent.Result.KICK_BANNED, KBanUtils.getBanMessageComponent(ban, event.getPlayer().getName()));
            }
        }
    }
}

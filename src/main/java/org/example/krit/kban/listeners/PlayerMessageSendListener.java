package org.example.krit.kban.listeners;

import org.bukkit.ChatColor;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.AsyncPlayerChatEvent;
import org.example.krit.kban.KBan;
import org.example.krit.kban.model.Mute;
import org.example.krit.kban.utils.KBanUtils;
import org.example.krit.kban.utils.MuteUtils;

import java.io.IOException;
import java.util.Objects;

import static org.example.krit.kban.utils.KBanUtils.millisecToTime;

public class PlayerMessageSendListener implements Listener {

    @EventHandler
    public void onPlayerMessageSend(AsyncPlayerChatEvent e) {
        Mute mute = MuteUtils.findMute(e.getPlayer().getUniqueId().toString());
        if (mute != null) {
            if (mute.getExpiresAt() < System.currentTimeMillis() && mute.getExpiresAt() != 0) {
                try {
                    MuteUtils.deleteMute(e.getPlayer().getUniqueId().toString());
                } catch (IOException ex) {
                    throw new RuntimeException(ex);
                }
                return;
            }

            String muteMessage = ChatColor.translateAlternateColorCodes('&', Objects.requireNonNull(KBan.getInstance().getConfig().getString("player-mute-chat-message"))
                    .replaceAll("<player>", KBanUtils.getBannedBy(mute.getMutedBy()))
                    .replaceAll("<time>", mute.getExpiresAt() == 0 ? "навсегда" : millisecToTime(mute.getExpiresAt() - System.currentTimeMillis()))
                    .replaceAll("<reason>", mute.getReason())
            );
            e.getPlayer().sendMessage(muteMessage);
            e.setCancelled(true);
        }
    }
}

package org.example.krit.kban.menusystem;

import org.bukkit.ChatColor;
import org.bukkit.Material;

public abstract class PaginatedMenu extends Menu {

    //Keep track of what page the menu is on
    protected int page = 0;
    //28 is max items because with the border set below,
    //28 empty slots are remaining.
    protected int maxItemsPerPage = 28;
    //the index represents the index of the slot
    //that the loop is on
    protected int index = 0;

    public PaginatedMenu(PlayerMenuUtility playerMenuUtility) {
        super(playerMenuUtility);
    }

    //Set the border and menu buttons for the menu
    public void addMenuBorder(int totalItems) {
        int totalPages = (int) Math.round(Math.ceil((double) (totalItems == 0 ? 1 : totalItems) / maxItemsPerPage));

        inventory.setItem(48, makeItem(Material.DARK_OAK_BUTTON, ChatColor.GREEN + "Влево", "Страница сейчас: " + ChatColor.WHITE + (page + 1), "Страниц всего: " + ChatColor.WHITE + totalPages));

        inventory.setItem(49, makeItem(Material.BARRIER, ChatColor.DARK_RED + "Назад"));

        inventory.setItem(50, makeItem(Material.DARK_OAK_BUTTON, ChatColor.GREEN + "Вправо", "Страница сейчас: " + ChatColor.WHITE + (page + 1), "Страниц всего: " + ChatColor.WHITE + totalPages));

        for (int i = 0; i < 10; i++) {
            if (inventory.getItem(i) == null) {
                inventory.setItem(i, super.FILLER_GLASS);
            }
        }

        inventory.setItem(17, super.FILLER_GLASS);
        inventory.setItem(18, super.FILLER_GLASS);
        inventory.setItem(26, super.FILLER_GLASS);
        inventory.setItem(27, super.FILLER_GLASS);
        inventory.setItem(35, super.FILLER_GLASS);
        inventory.setItem(36, super.FILLER_GLASS);

        for (int i = 44; i < 54; i++) {
            if (inventory.getItem(i) == null) {
                inventory.setItem(i, super.FILLER_GLASS);
            }
        }
        Material[] totalPagesMaterial = {Material.LIME_GLAZED_TERRACOTTA, Material.GREEN_GLAZED_TERRACOTTA, Material.YELLOW_GLAZED_TERRACOTTA, Material.ORANGE_GLAZED_TERRACOTTA, Material.RED_GLAZED_TERRACOTTA};
        inventory.setItem(4, makeItem(totalPagesMaterial[Math.min(page, totalPagesMaterial.length - 1)],  ChatColor.RED + "Всего: " + ChatColor.WHITE + totalItems));
    }

    public int getMaxItemsPerPage() {
        return maxItemsPerPage;
    }
}

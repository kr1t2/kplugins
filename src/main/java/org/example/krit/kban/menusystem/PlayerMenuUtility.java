package org.example.krit.kban.menusystem;

import org.bukkit.OfflinePlayer;
import org.bukkit.entity.Player;

public class PlayerMenuUtility {

    private Player owner;
    private OfflinePlayer playerTarget;

    public PlayerMenuUtility(Player p) {
        this.owner = p;
    }
    public PlayerMenuUtility(Player p, OfflinePlayer playerTarget) {
        this.owner = p;
        this.playerTarget = playerTarget;
    }

    public Player getOwner() {
        return owner;
    }

    public OfflinePlayer getPlayerTarget() {
        return playerTarget;
    }

    public void setPlayerTarget(OfflinePlayer playerToKill) {
        this.playerTarget = playerToKill;
    }


}

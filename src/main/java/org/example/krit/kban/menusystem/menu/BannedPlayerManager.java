package org.example.krit.kban.menusystem.menu;

import net.md_5.bungee.api.ChatColor;
import org.bukkit.BanList;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.ban.ProfileBanList;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.SkullMeta;
import org.example.krit.kban.KBan;
import org.example.krit.kban.menusystem.Menu;
import org.example.krit.kban.menusystem.PlayerMenuUtility;
import org.example.krit.kban.model.Ban;
import org.example.krit.kban.model.Mute;
import org.example.krit.kban.model.PlayerAddress;
import org.example.krit.kban.utils.BanListUtils;
import org.example.krit.kban.utils.KBanUtils;
import org.example.krit.kban.utils.MuteUtils;
import org.example.krit.kban.utils.PlayerAddressUtils;

import java.io.IOException;
import java.util.List;
import java.util.Objects;

public class BannedPlayerManager extends Menu {
    public BannedPlayerManager(PlayerMenuUtility playerMenuUtility) {
        super(playerMenuUtility);
    }

    @Override
    public String getMenuName() {
        return "Игрок " + playerMenuUtility.getPlayerTarget().getName();
    }

    @Override
    public int getSlots() {
        return 27;
    }

    @Override
    public void handleMenu(InventoryClickEvent e) {
        Player p = (Player) e.getWhoClicked();
        switch (e.getCurrentItem().getType()) {
            case BARRIER:
                // Назад идём
                new Banlist(new PlayerMenuUtility((Player) e.getWhoClicked())).open();;
                break;
            case GREEN_BANNER:
                if (!p.hasPermission("kban.unban")) {
                    return;
                }

                if (BanListUtils.findBan(playerMenuUtility.getPlayerTarget().getUniqueId().toString()) == null) {
                    return;
                }

                // Разбаниваем игрока
                try {
                    BanListUtils.unban(playerMenuUtility.getPlayerTarget().getUniqueId().toString());
                } catch (IOException exception) {
                    return;
                }
                // Также разбаниваем его с ванильной бан системы
                ProfileBanList profileBanList = Bukkit.getBanList(BanList.Type.PROFILE);
                profileBanList.pardon(playerMenuUtility.getPlayerTarget().getPlayerProfile());

                // Отправляем сообщение об успешном разбане
                e.getWhoClicked().sendMessage(org.bukkit.ChatColor.translateAlternateColorCodes('&', Objects.requireNonNull(Objects.requireNonNull(KBan.getInstance().getConfig().getString("player-unban-message"))
                        .replaceAll("<player>", Objects.requireNonNull(playerMenuUtility.getPlayerTarget().getName()))
                )));
                inventory.clear();
                setMenuItems();
                break;

            case PAPER:

                //закрыть
                p.closeInventory();

                // Открыть другой инвентарь
                new PlayerWarnsList(playerMenuUtility).open();

                break;
            case IRON_BARS:
                if (!p.hasPermission("kban.unmute")) {
                    return;
                }

                if (MuteUtils.findMute(playerMenuUtility.getPlayerTarget().getUniqueId().toString()) == null) {
                    return;
                }
                
                KBanUtils.unmute(playerMenuUtility.getPlayerTarget(), p.getUniqueId().toString());

                inventory.clear();
                setMenuItems();
                break;
        }
    }

    @Override
    public void setMenuItems() {
        setFillerGlass();

        SkullMeta skull = (SkullMeta) Bukkit.getItemFactory().getItemMeta(Material.PLAYER_HEAD);
        skull.setOwningPlayer(playerMenuUtility.getPlayerTarget());
        skull.setDisplayName(playerMenuUtility.getPlayerTarget().getName());

        Ban ban = BanListUtils.findBan(playerMenuUtility.getPlayerTarget().getUniqueId().toString());
        if (ban != null) {
            String banExpires = ban.getBanExpires() == 0 ? "навсегда" : KBanUtils.millisecToTime(ban.getBanExpires() - System.currentTimeMillis());
            skull.setLore(List.of(
                    ChatColor.RED + "Забанен: " + ChatColor.WHITE + KBanUtils.millisecToTime(System.currentTimeMillis() - ban.getBannedDate()) + " назад",
                    ChatColor.RED + "Заканчивается через: " + ChatColor.WHITE + banExpires,
                    ChatColor.RED + "Тип бана: " + (ban.isIpBan() ? "По IP Адресу: " + ChatColor.WHITE + ban.getIp() : " По UUID игрока: " + ChatColor.WHITE + ban.getUuid()),
                    ChatColor.RED + "Причина: " + ChatColor.WHITE + ban.getReason(),
                    ChatColor.RED + "Забанил: " + org.bukkit.ChatColor.WHITE + KBanUtils.getBannedBy(ban.getBannedBy())
            ));
        } else {
            skull.setLore(List.of(ChatColor.GREEN + "Не в бане"));
        }
        Mute mute = MuteUtils.findMute(playerMenuUtility.getPlayerTarget().getUniqueId().toString());

        ItemStack item = new ItemStack(Material.PLAYER_HEAD);
        item.setItemMeta(skull);;

        inventory.setItem(4, item);
        inventory.setItem(22, makeItem(Material.BARRIER, ChatColor.RED + "Назад"));
        PlayerAddress address = PlayerAddressUtils.findAddress(playerMenuUtility.getPlayerTarget().getUniqueId().toString());
        if (mute == null) {
            inventory.setItem(10, makeItem(Material.LIME_GLAZED_TERRACOTTA, ChatColor.GREEN + "Не в муте"));
        } else {
            String muteExpires = mute.getExpiresAt() == 0 ? "навсегда" : KBanUtils.millisecToTime(mute.getExpiresAt() - System.currentTimeMillis());
            inventory.setItem(10, makeItem(Material.IRON_BARS, ChatColor.RED + "В муте",
                    org.bukkit.ChatColor.RED + "Заканчивается через: " + org.bukkit.ChatColor.WHITE + muteExpires,
                    org.bukkit.ChatColor.RED + "Причина: " + org.bukkit.ChatColor.WHITE + mute.getReason(),
                    org.bukkit.ChatColor.RED + "Замьютил кто: " + org.bukkit.ChatColor.WHITE + KBanUtils.getBannedBy(mute.getMutedBy()),
                    "Нажмите чтобы размьютить"
            ));
        }

        if (ban != null) {
            inventory.setItem(11, makeItem(Material.GREEN_BANNER, ChatColor.GREEN + "Разбанить"));
        } else {
            inventory.setItem(11, makeItem(Material.GREEN_BANNER, ChatColor.GREEN + "Не в бане"));
        }

        inventory.setItem(15, makeItem(Material.PAPER, ChatColor.GREEN + "Список предупреждений"));
        inventory.setItem(16, makeItem(Material.LEVER, ChatColor.BLUE + "Другая информация", ChatColor.WHITE + "UUID: " + org.bukkit.ChatColor.LIGHT_PURPLE + playerMenuUtility.getPlayerTarget().getUniqueId().toString(), ChatColor.WHITE + "IP Адрес: " + ChatColor.LIGHT_PURPLE + (address == null ? "Не сохранён" : address.getAddress())));
    }
}

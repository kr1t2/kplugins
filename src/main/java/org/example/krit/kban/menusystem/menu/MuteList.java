package org.example.krit.kban.menusystem.menu;

import org.bukkit.*;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.SkullMeta;
import org.bukkit.persistence.PersistentDataType;
import org.example.krit.kban.KBan;
import org.example.krit.kban.menusystem.PaginatedMenu;
import org.example.krit.kban.menusystem.PlayerMenuUtility;
import org.example.krit.kban.model.Ban;
import org.example.krit.kban.model.Mute;
import org.example.krit.kban.utils.BanListUtils;
import org.example.krit.kban.utils.KBanUtils;
import org.example.krit.kban.utils.MuteUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.UUID;

public class MuteList extends PaginatedMenu {
    public MuteList(PlayerMenuUtility playerMenuUtility) {
        super(playerMenuUtility);
    }

    @Override
    public String getMenuName() {
        return "Мутлист";
    }

    @Override
    public int getSlots() {
        return 54;
    }
    @Override
    public void handleMenu(InventoryClickEvent e) {
        Player p = (Player) e.getWhoClicked();

        ArrayList<String> players = new ArrayList<>();
        for (Mute mute : MuteUtils.getMutesList()) {
            players.add(mute.getUuid());
        }

        if (e.getCurrentItem().getType().equals(Material.PLAYER_HEAD)) {

            PlayerMenuUtility playerMenuUtility = KBan.getPlayerMenuUtility(p);
            playerMenuUtility.setPlayerTarget(Bukkit.getOfflinePlayer(UUID.fromString(Objects.requireNonNull(e.getCurrentItem().getItemMeta().getPersistentDataContainer().get(new NamespacedKey(KBan.getInstance(), "uuid"), PersistentDataType.STRING)))));

            new BannedPlayerManager(playerMenuUtility).open();

        }else if (e.getCurrentItem().getType().equals(Material.BARRIER)) {

            //close inventory
            p.closeInventory();

        }else if(e.getCurrentItem().getType().equals(Material.DARK_OAK_BUTTON)){
            if (ChatColor.stripColor(e.getCurrentItem().getItemMeta().getDisplayName()).equalsIgnoreCase("Влево")){
                if (page != 0) {
                    page = page - 1;
                    super.open();
                }
            }else if (ChatColor.stripColor(e.getCurrentItem().getItemMeta().getDisplayName()).equalsIgnoreCase("Вправо")){
                if (!((index + 1) >= players.size())){
                    page = page + 1;
                    super.open();
                }
            }
        }
    }

    @Override
    public void setMenuItems() {

        ArrayList<String> players = new ArrayList<>();
        for (Mute mutedPlayer : MuteUtils.getMutesList()) {
            players.add(mutedPlayer.getUuid());
        }
        addMenuBorder(players.size());

        if(!players.isEmpty()) {
            for(int i = 0; i < getMaxItemsPerPage(); i++) {
                index = getMaxItemsPerPage() * page + i;
                if(index >= players.size()) break;
                if (players.get(index) != null){

                    SkullMeta skull = (SkullMeta) Bukkit.getItemFactory().getItemMeta(Material.PLAYER_HEAD);
                    skull.setOwningPlayer(Bukkit.getOfflinePlayer(players.get(index)));
                    OfflinePlayer player = Bukkit.getOfflinePlayer(UUID.fromString(players.get(index)));
                    skull.setDisplayName(player.getName());

                    Mute mute = MuteUtils.findMute(players.get(index));
                    assert mute != null;

                    String muteExpires = mute.getExpiresAt() == 0 ? "навсегда" : KBanUtils.millisecToTime(mute.getExpiresAt() - System.currentTimeMillis());
                    skull.setLore(List.of(
                            ChatColor.RED + "Заканчивается через: " + ChatColor.WHITE + muteExpires,
                            ChatColor.RED + "Причина: " + ChatColor.WHITE + mute.getReason(),
                            ChatColor.RED + "Замьютил кто: " + ChatColor.WHITE + KBanUtils.getBannedBy(mute.getMutedBy())
                    ));

                    skull.getPersistentDataContainer().set(new NamespacedKey(KBan.getInstance(), "uuid"), PersistentDataType.STRING, players.get(index));

                    ItemStack item = new ItemStack(Material.PLAYER_HEAD);
                    item.setItemMeta(skull);;

                    inventory.addItem(item);

                }
            }
        }
    }
}

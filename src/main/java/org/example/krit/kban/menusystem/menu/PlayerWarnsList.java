package org.example.krit.kban.menusystem.menu;

import org.bukkit.*;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.SkullMeta;
import org.bukkit.persistence.PersistentDataType;
import org.example.krit.kban.KBan;
import org.example.krit.kban.menusystem.PaginatedMenu;
import org.example.krit.kban.menusystem.PlayerMenuUtility;
import org.example.krit.kban.model.Warn;
import org.example.krit.kban.model.Warns;
import org.example.krit.kban.utils.KBanUtils;
import org.example.krit.kban.utils.WarnsUtils;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.UUID;

public class PlayerWarnsList extends PaginatedMenu {

    public PlayerWarnsList(PlayerMenuUtility playerMenuUtility) {
        super(playerMenuUtility);
    }

    @Override
    public String getMenuName() {
        return "Варнлист игрока " + playerMenuUtility.getPlayerTarget().getName();
    }

    @Override
    public int getSlots() {
        return 54;
    }
    @Override
    public void handleMenu(InventoryClickEvent e) {
        Player p = (Player) e.getWhoClicked();

        ArrayList<Warn> warns = new ArrayList<>();
        if (WarnsUtils.findWarns(playerMenuUtility.getPlayerTarget().getUniqueId().toString()) != null) {
            warns.addAll(Objects.requireNonNull(WarnsUtils.findWarns(playerMenuUtility.getPlayerTarget().getUniqueId().toString())).getWarns());
        }

        if (e.getCurrentItem().getType().equals(Material.BARRIER)) {

            //close inventory
            p.closeInventory();

            new BannedPlayerManager(playerMenuUtility).open();

        }else if(e.getCurrentItem().getType().equals(Material.DARK_OAK_BUTTON)){
            if (ChatColor.stripColor(e.getCurrentItem().getItemMeta().getDisplayName()).equalsIgnoreCase("Влево")){
                if (page != 0) {
                    page = page - 1;
                    super.open();
                }
            }else if (ChatColor.stripColor(e.getCurrentItem().getItemMeta().getDisplayName()).equalsIgnoreCase("Вправо")){
                if (!((index + 1) >= warns.size())){
                    page = page + 1;
                    super.open();
                }
            }
        } else if (e.getCurrentItem().getType() == Material.PLAYER_HEAD) {
            // Удалить варн
            if (!e.getWhoClicked().hasPermission("kban.warn")) {
                p.sendMessage(ChatColor.translateAlternateColorCodes('&', Objects.requireNonNull(KBan.getInstance().getConfig().getString("no-permission-message"))));
                return;
            }
            OfflinePlayer player = playerMenuUtility.getPlayerTarget();
            Warns playerWarns = WarnsUtils.findWarns(player.getUniqueId().toString());
            assert playerWarns != null;

            ArrayList<Warn> updatedWarns = playerWarns.getWarns();
            updatedWarns.remove(updatedWarns.get(e.getCurrentItem().getItemMeta().getPersistentDataContainer().get(new NamespacedKey(KBan.getInstance(), "number"), PersistentDataType.INTEGER)));

            for (int i = 0; i < updatedWarns.size(); i++) {
                updatedWarns.get(i).setNumber(i + 1);
            }
            try {
                WarnsUtils.deleteWarns(playerMenuUtility.getPlayerTarget().getUniqueId().toString());
                WarnsUtils.addWarns(playerMenuUtility.getPlayerTarget().getUniqueId().toString(), updatedWarns);
                e.getWhoClicked().sendMessage(ChatColor.translateAlternateColorCodes('&', Objects.requireNonNull(Objects.requireNonNull(KBan.getInstance().getConfig().getString("delete-warn-message"))
                        .replaceAll("<player>", Objects.requireNonNull(player.getName())))
                        .replaceAll("<warnNum>", String.valueOf(warns.size()))
                ));
            } catch (IOException ex) {
                throw new RuntimeException(ex);
            }
            inventory.clear();
            setMenuItems();

        }
    }

    @Override
    public void setMenuItems() {

        if (WarnsUtils.findWarns(playerMenuUtility.getPlayerTarget().getUniqueId().toString()) == null) {
            addMenuBorder(0);
            return;
        }

        ArrayList<Warn> warns = new ArrayList<>();
        for (Warn warn : Objects.requireNonNull(WarnsUtils.findWarns(playerMenuUtility.getPlayerTarget().getUniqueId().toString())).getWarns()) {
            warns.add(warn);
        }

        addMenuBorder(warns.size());

        if(!warns.isEmpty()) {
            for(int i = 0; i < getMaxItemsPerPage(); i++) {
                index = getMaxItemsPerPage() * page + i;
                if(index >= warns.size()) break;
                if (warns.get(index) != null){

                    SkullMeta skull = (SkullMeta) Bukkit.getItemFactory().getItemMeta(Material.PLAYER_HEAD);
                    skull.setOwningPlayer(Bukkit.getOfflinePlayer(UUID.fromString(warns.get(index).getSenderUUID())));
                    skull.setDisplayName(ChatColor.RED + "Варн #"+ ChatColor.WHITE + warns.get(index).getNumber() + ChatColor.RED + " от: " + ChatColor.WHITE + Bukkit.getOfflinePlayer(UUID.fromString(warns.get(index).getSenderUUID())).getName());
                    skull.setLore(List.of(
                            ChatColor.RED + "Выдан: " + ChatColor.WHITE + KBanUtils.millisecToTime((System.currentTimeMillis() - warns.get(index).getGivenAt())) + ChatColor.RED + " назад",
                            ChatColor.RED + "Причина: " + ChatColor.WHITE + warns.get(index).getContent(),
                            "Нажмите чтобы удалить"
                    ));
                    skull.getPersistentDataContainer().set(new NamespacedKey(KBan.getInstance(), "number"), PersistentDataType.INTEGER, index);
                    ItemStack item = new ItemStack(Material.PLAYER_HEAD);
                    item.setItemMeta(skull);;

                    inventory.addItem(item);

                }
            }
        }
    }
}

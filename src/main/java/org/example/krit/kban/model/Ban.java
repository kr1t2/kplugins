package org.example.krit.kban.model;

public class Ban {

    private String uuid;
    private long banExpires;
    private long bannedDate;
    private String bannedBy;
    private String reason;
    private boolean ipBan;
    private String ip;

    public Ban(String uuid, long expires, long bannedDate, String bannedBy, String reason, boolean ipBan, String ip) {

        this.uuid = uuid;
        this.banExpires = expires;
        this.bannedDate = bannedDate;
        this.bannedBy = bannedBy;
        this.reason = reason;
        this.ipBan = ipBan;
        this.ip = ip;
    }


    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public long getBanExpires() {
        return banExpires;
    }

    public void setBanExpires(long banExpires) {
        this.banExpires = banExpires;
    }

    public String getBannedBy() {
        return bannedBy;
    }

    public void setBannedBy(String bannedBy) {
        this.bannedBy = bannedBy;
    }
    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    public boolean isIpBan() {
        return ipBan;
    }

    public void setIpBan(boolean ipBan) {
        this.ipBan = ipBan;
    }

    public long getBannedDate() {
        return bannedDate;
    }

    public void setBannedDate(long bannedDate) {
        this.bannedDate = bannedDate;
    }

    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }
}
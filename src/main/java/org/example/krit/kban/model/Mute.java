package org.example.krit.kban.model;

public class Mute {

    private String uuid;
    private String mutedBy;
    private String reason;
    private long expiresAt;

    public Mute(String uuid, String mutedBy, String reason, long expiresAt) {

        this.uuid = uuid;
        this.mutedBy = mutedBy;
        this.reason = reason;
        this.expiresAt = expiresAt;
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public String getMutedBy() {
        return mutedBy;
    }

    public void setMutedBy(String mutedBy) {
        this.mutedBy = mutedBy;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    public long getExpiresAt() {
        return expiresAt;
    }

    public void setExpiresAt(long expiresAt) {
        this.expiresAt = expiresAt;
    }
}

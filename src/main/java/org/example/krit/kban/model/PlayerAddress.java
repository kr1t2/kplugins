package org.example.krit.kban.model;

public class PlayerAddress {

    private String playerUUID;
    private String address;

    public PlayerAddress(String playerUUID, String address) {

        this.playerUUID = playerUUID;
        this.address = address;
    }

    public String getPlayerUUID() {
        return playerUUID;
    }

    public void setPlayerUUID(String playerUUID) {
        this.playerUUID = playerUUID;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }
}

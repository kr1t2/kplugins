package org.example.krit.kban.model;

public class Warn {

    private String senderUUID;
    private String content;
    private long givenAt;
    private int number;

    public Warn(String senderUUID, String content, long givenAt, int number) {

        this.senderUUID = senderUUID;
        this.content = content;
        this.givenAt = givenAt;
        this.number = number;
    }

    public String getSenderUUID() {
        return senderUUID;
    }

    public void setSenderUUID(String senderUUID) {
        this.senderUUID = senderUUID;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public long getGivenAt() {
        return givenAt;
    }

    public int getNumber() {
        return number;
    }

    public void setNumber(int number) {
        this.number = number;
    }
}

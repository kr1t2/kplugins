package org.example.krit.kban.model;

import java.util.ArrayList;

public class Warns {

    private String playerUUID;
    private ArrayList<Warn> warns;

    public Warns(String playerUUID, ArrayList<Warn>warns) {

        this.playerUUID = playerUUID;
        this.warns = warns;
    }

    public ArrayList<Warn> getWarns() {
        return warns;
    }

    public void setWarns(ArrayList<Warn> warns) {
        this.warns = warns;
    }

    public String getPlayerUUID() {
        return playerUUID;
    }

    public void setPlayerUUID(String playerUUID) {
        this.playerUUID = playerUUID;
    }
}

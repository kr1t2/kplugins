package org.example.krit.kban.utils;

import com.google.gson.Gson;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.example.krit.kban.KBan;
import org.example.krit.kban.model.Ban;

import java.io.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;

public class BanListUtils {

    private static ArrayList<Ban> bans = new ArrayList<>();

    public static Ban ban(String playerUUID, long expires, String bannedBy, String reason, boolean ipBan, String ip) {

        Ban ban = new Ban(playerUUID, expires == 0 ? 0 : System.currentTimeMillis() + expires, System.currentTimeMillis(), bannedBy, reason, ipBan, ip);
        bans.add(ban);
        try {
            updateBan(playerUUID, ban);
        } catch (IOException e) {
            Bukkit.getConsoleSender().sendMessage(ChatColor.translateAlternateColorCodes('&', Objects.requireNonNull(KBan.getInstance().getConfig().getString("error-message"))));
            throw new RuntimeException(e);
        }

        return ban;
    }

    public static void unban(String playerUUID) throws IOException {
        for (Ban ban : bans) {
            if (ban.getUuid().equals(playerUUID)) {
                bans.remove(ban);
                saveBanList();
                break;
            }
        }

    }

    public static Ban findBan(String playerUUID) {
        for (Ban ban : bans) {
            if (ban.getUuid().equals(playerUUID)) {
                return ban;
            }
        }
        return null;
    }

    public static Ban updateBan(String playerUUID, Ban banToUpdate) throws IOException {
        for (Ban ban : bans) {
            if (ban.getUuid().equals(playerUUID)) {

                ban.setBannedBy(banToUpdate.getBannedBy());
                ban.setIpBan(banToUpdate.isIpBan());
                ban.setBanExpires(banToUpdate.getBanExpires());
                ban.setBannedDate(banToUpdate.getBannedDate());
                ban.setUuid(banToUpdate.getUuid());
                ban.setReason(banToUpdate.getReason());

                saveBanList();
                break;
            }
        }
        return null;
    }

    public static List<Ban> getBanList() {
        return bans;
    }

    public static void loadBanList() throws IOException {

        Gson gson = new Gson();
        File file = new File(KBan.getInstance().getDataFolder().getAbsolutePath() + "/banlist.json");
        if (file.exists()) {
            Reader reader = new FileReader(file);
            Ban[] n = gson.fromJson(reader, Ban[].class);
            bans = new ArrayList<>(Arrays.asList(n));
        }

    }

    public static void saveBanList() throws IOException {

        Gson gson = new Gson();
        File file = new File(KBan.getInstance().getDataFolder().getAbsolutePath() + "/banlist.json");
        file.getParentFile().mkdir();
        file.createNewFile();
        Writer writer = new FileWriter(file, false);
        gson.toJson(bans, writer);
        writer.flush();
        writer.close();

    }

}
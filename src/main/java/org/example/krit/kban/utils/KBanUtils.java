package org.example.krit.kban.utils;

import com.destroystokyo.paper.profile.PlayerProfile;
import io.papermc.paper.ban.BanListType;
import it.unimi.dsi.fastutil.objects.Object2ReferenceArrayMap;
import net.kyori.adventure.text.Component;
import org.bukkit.*;
import org.bukkit.ban.ProfileBanList;
import org.bukkit.command.CommandSender;
import org.bukkit.command.ConsoleCommandSender;
import org.bukkit.entity.Player;
import org.example.krit.kban.KBan;
import org.example.krit.kban.model.Ban;
import org.example.krit.kban.model.PlayerAddress;
import org.example.krit.kban.model.Warn;
import org.example.krit.kban.model.Warns;

import java.io.IOException;
import java.time.Instant;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.UUID;
import java.util.concurrent.TimeUnit;

public class KBanUtils {
    public static void send(CommandSender sender, String message) {
        if (sender instanceof Player p) {
            p.sendMessage(message);
        } else if (sender instanceof ConsoleCommandSender) {
            Bukkit.getConsoleSender().sendMessage(message);
        }
    }

    public static Ban ban(OfflinePlayer target, CommandSender sender, String reason, long expiresAt, boolean ipBan, String address) {

        if (reason.isEmpty()) {
            reason = "Причина не указана";
        }

        if (BanListUtils.findBan(target.getUniqueId().toString()) != null) {
            try {
                BanListUtils.unban(target.getUniqueId().toString());
            } catch (IOException e) {
                Bukkit.getConsoleSender().sendMessage(ChatColor.translateAlternateColorCodes('&', Objects.requireNonNull(KBan.getInstance().getConfig().getString("error-message"))));
                throw new RuntimeException(e);
            }
        }

        // Баним игрока
        String bannedBy = sender instanceof Player p ? p.getUniqueId().toString() : "console";

        // Сохраняем в файле JSON
        Ban ban = BanListUtils.ban(target.getUniqueId().toString(), expiresAt, bannedBy, reason, ipBan, address);

        if (target.isOnline()) {
            target.getPlayer().kick(getBanMessageComponent(ban, target.getName()));
        }
        // Баним с ванильной бан системы ещё
        Bukkit.getBanList(BanListType.PROFILE).addBan(target.getPlayerProfile(), reason, Instant.ofEpochMilli(expiresAt), null);


        if (KBan.getInstance().getConfig().getBoolean("send-to-everyone.bans")) {
            if (ipBan) {
                MessagesUtils.sendBanIpMessage(ban);
            } else {
                MessagesUtils.sendBanMessage(ban);
            }
        } else {
            if (ipBan) {
                MessagesUtils.sendBanIpMessage(sender, ban);
            } else {
                MessagesUtils.sendBanMessage(ban, sender);
            }
        }
        
        return ban;
    }

    public static void ban(OfflinePlayer target, String reason, long expiresAt, boolean ipBan, String address) {

        if (reason.isEmpty()) {
            reason = "Причина не указана";
        }

        if (BanListUtils.findBan(target.getUniqueId().toString()) != null) {
            try {
                BanListUtils.unban(target.getUniqueId().toString());
            } catch (IOException e) {
                Bukkit.getConsoleSender().sendMessage(ChatColor.translateAlternateColorCodes('&', Objects.requireNonNull(KBan.getInstance().getConfig().getString("error-message"))));
                throw new RuntimeException(e);
            }
        }

        // Баним игрока
        String bannedBy = "console";
        // Сохраняем в файле JSON
        Ban ban = BanListUtils.ban(target.getUniqueId().toString(), expiresAt, bannedBy, reason, ipBan, address);

        if (target.isOnline()) {
            target.getPlayer().kick(getBanMessageComponent(ban, target.getName()));
            if (ipBan && address != null) {
                for (Player people : Bukkit.getOnlinePlayers()) {
                    if (Objects.requireNonNull(people.getAddress()).getAddress().getHostName().equals(address)) {
                        people.kick(getBanMessageComponent(ban, target.getName()));
                    }
                }
            }
        }

        // Баним с ванильной бан системы ещё
        Bukkit.getBanList(BanListType.PROFILE).addBan(target.getPlayerProfile(), reason, Instant.ofEpochMilli(expiresAt), null);
    }

    public static void unban(String playerUUID) {
        // 3. Далее разбан игрока
        try {
            BanListUtils.unban(playerUUID);
        } catch (IOException e) {
            return;
        }
        // Также разбаниваем его с ванильной бан системы
        ProfileBanList profileBanList = Bukkit.getBanList(BanList.Type.PROFILE);
        profileBanList.pardon(Bukkit.getOfflinePlayer(UUID.fromString(playerUUID)).getPlayerProfile());
    }


    public static String millisecToTime(long milliseconds) {
        final long day = TimeUnit.MILLISECONDS.toDays(milliseconds);

        final long hours = TimeUnit.MILLISECONDS.toHours(milliseconds)
                - TimeUnit.DAYS.toHours(TimeUnit.MILLISECONDS.toDays(milliseconds));

        final long minutes = TimeUnit.MILLISECONDS.toMinutes(milliseconds)
                - TimeUnit.HOURS.toMinutes(TimeUnit.MILLISECONDS.toHours(milliseconds));

        final long seconds = TimeUnit.MILLISECONDS.toSeconds(milliseconds)
                - TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(milliseconds));

        if (seconds == 0 && minutes == 0 && hours == 0 && day == 0) {
            return "Только что";
        } else if (minutes == 0 && hours == 0 && day == 0) {
            return String.format("%d s.", seconds);
        } else if (hours == 0 && day == 0) {
            return String.format("%d m. %d s.", minutes, seconds);
        } else if (day == 0) {
            return String.format("%d h. %d m. %d s.", hours, minutes, seconds);
        } else {
            return String.format("%d d. %d h. %d m. %d s.", day, hours, minutes, seconds);
        }
    }

    public static void checkWarns(String uuid, CommandSender sender) {
        // Баним игрока за большое количество предупреждений
        Warns warns = WarnsUtils.findWarns(uuid);
        assert warns != null;

        OfflinePlayer target = Bukkit.getOfflinePlayer(UUID.fromString(uuid));
        ArrayList<Warn> warnsArray = warns.getWarns();
        if (warnsArray.size() > KBan.getInstance().getConfig().getInt("warns.warns-need-to-ban")) {
            if (KBan.getInstance().getConfig().getBoolean("warns.ip-ban-for-warns")) {
                PlayerAddress address = PlayerAddressUtils.findAddress(target.getUniqueId().toString());
                KBanUtils.ban(target, "Большое количество предупреждений: " + warnsArray.size(), KBan.getInstance().getConfig().getInt("warns.ban-for-warns-time") * 3600000L, true, address == null ? null : address.getAddress());
                KBanUtils.send(sender, ChatColor.translateAlternateColorCodes('&', Objects.requireNonNull(Objects.requireNonNull(KBan.getInstance().getConfig().getString("warns-ip-ban-message"))
                        .replaceAll("<player>", Objects.requireNonNull(target.getName()))
                        .replaceAll("<warnNum>", String.valueOf(warnsArray.size()))
                        .replaceAll("<address>", address == null ? "не сохранено - будет забанен при следующем входе" : address.getAddress())
                        .replaceAll("<needWarnsToBan>", String.valueOf(KBan.getInstance().getConfig().getInt("warns.warns-need-to-ban")))
                        .replaceAll("<time>", KBanUtils.millisecToTime(KBan.getInstance().getConfig().getInt("warns.ban-for-warns-time") * 3600000L))
                )));
            } else if (KBan.getInstance().getConfig().getBoolean("warns.ban-for-warns")) {
                KBanUtils.ban(target, "Большое количество предупреждений: " + warnsArray.size(), (long) (KBan.getInstance().getConfig().getDouble("warns.ban-for-warns-time") * 3600000L), false, null);
                KBanUtils.send(sender, ChatColor.translateAlternateColorCodes('&', Objects.requireNonNull(Objects.requireNonNull(KBan.getInstance().getConfig().getString("warns-ban-message"))
                        .replaceAll("<player>", Objects.requireNonNull(target.getName()))
                        .replaceAll("<warnNum>", String.valueOf(warnsArray.size()))
                        .replaceAll("<needWarnsToBan>", String.valueOf(KBan.getInstance().getConfig().getInt("warns.warns-need-to-ban")))
                        .replaceAll("<time>", KBanUtils.millisecToTime(KBan.getInstance().getConfig().getInt("warns.ban-for-warns-time") * 3600000L))
                )));
            }
        }
    }
    public static void convertMojangBansToKBan() {
        for (BanEntry entry : Bukkit.getBanList(BanList.Type.PROFILE).getEntries()) {
            if (BanListUtils.findBan(Objects.requireNonNull(((PlayerProfile) entry.getBanTarget()).getId()).toString()) == null) {
                BanListUtils.ban(Objects.requireNonNull(((PlayerProfile) entry.getBanTarget()).getId()).toString(), Objects.requireNonNull(entry.getExpiration()).getTime(), "KBanConvertor", entry.getReason(), false, null);
            }
        }
    }

    public static String getBannedBy(String uuid) {
        return uuid.equals("KBanConvertor") || uuid.equals("console") ? uuid : Bukkit.getOfflinePlayer(UUID.fromString(uuid)).getName();
    }

    public static void kickPlayer(Player playerToKick, String kickedByName, String reason) {
        playerToKick.kick(getKickMessageComponent(playerToKick, kickedByName, reason));
    }

    public static Component getBanMessageComponent(Ban ban, String name) {
        List<String> banMessage = KBan.getInstance().getConfig().getStringList("ban"+ (ban.isIpBan() ? "ip" : "") + "-message");
        List<Component> componentsBanMessage = new ArrayList<>();
        for (String message : banMessage) {
            StringBuilder playersWithTheSameIp = new StringBuilder();
            if (ban.isIpBan()) {
                for (Ban ipban : BanListUtils.getBanList()) {
                    if (ban.getIp() != null && ban.getIp().equals(ipban.getIp())) {
                        playersWithTheSameIp.append(Bukkit.getOfflinePlayer(UUID.fromString(ipban.getUuid())).getName()).append(", ");
                    }
                }
            }
            componentsBanMessage.add(Component.text(ChatColor.translateAlternateColorCodes('&', message
                    .replaceAll("<bannedBy>", KBanUtils.getBannedBy(ban.getBannedBy()))
                    .replaceAll("<expiresTime>", ban.getBanExpires() == 0 ? "никогда" : KBanUtils.millisecToTime(ban.getBanExpires() - System.currentTimeMillis()))
                    .replaceAll("<timeAgo>", KBanUtils.millisecToTime(System.currentTimeMillis() - ban.getBannedDate()))
                    .replaceAll("<reason>", ban.getReason())
                    .replaceAll("<player>", name)
                    .replaceAll("<address>", ban.getIp())
                    .replaceAll("<playersWithBannedIP>", playersWithTheSameIp.toString())
            )).appendNewline());
        }

        return Component.text().append(componentsBanMessage).build();
    }

    public static Component getKickMessageComponent(Player playerToKick, String kickedByName, String reason) {
        List<String> kickMessage = KBan.getInstance().getConfig().getStringList("kick-message");
        List<Component> componentsKickMessage = new ArrayList<>();
        for (String message : kickMessage) {
            componentsKickMessage.add(Component.text(ChatColor.translateAlternateColorCodes('&', message
                    .replaceAll("<kickedBy>", kickedByName)
                    .replaceAll("<reason>", reason)
                    .replaceAll("<player>", playerToKick.getName())
            )).appendNewline());
        }

        return Component.text().append(componentsKickMessage).build();
    }

    public static void mute(OfflinePlayer target, String mutedBy, String reason, long expiresAt) {
        if (MuteUtils.findMute(target.getUniqueId().toString()) != null) {
            try {
                MuteUtils.deleteMute(target.getUniqueId().toString());
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
        }
        MuteUtils.mute(target.getUniqueId().toString(), mutedBy, reason, expiresAt);
        String muteLogMessage = ChatColor.translateAlternateColorCodes('&', Objects.requireNonNull(KBan.getInstance().getConfig().getString("player-mute-log-message"))
                .replaceAll("<player>",  Objects.requireNonNull(Bukkit.getPlayer(UUID.fromString(mutedBy))).getName())
                .replaceAll("<time>", expiresAt == 0 ? "навсегда" : millisecToTime(expiresAt - System.currentTimeMillis()))
                .replaceAll("<reason>", reason)
        );
        String muteMessage = ChatColor.translateAlternateColorCodes('&', Objects.requireNonNull(KBan.getInstance().getConfig().getString("player-mute-message"))
                .replaceAll("<player>", Objects.requireNonNull(target.getName()))
                .replaceAll("<time>", expiresAt == 0 ? "навсегда" : millisecToTime(expiresAt - System.currentTimeMillis()))
                .replaceAll("<reason>", reason)
        );
        if (muteMessage.equals("console")) {
            Bukkit.getConsoleSender().sendMessage(muteLogMessage);
        } else {
            Player p = Bukkit.getPlayer(UUID.fromString(mutedBy));
            if (p != null) {
                p.sendMessage(muteLogMessage);
            }
        }
        if (target.isOnline()) {
            ((Player) target).sendMessage(muteMessage);
        }
    }
    public static void unmute(OfflinePlayer target, String unmutedBy) {
        try {
            MuteUtils.deleteMute(target.getUniqueId().toString());
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        String unmuteMessage = ChatColor.translateAlternateColorCodes('&', Objects.requireNonNull(KBan.getInstance().getConfig().getString("player-unmute-log-message"))
                .replaceAll("<player>", Objects.requireNonNull(target.getName()))
        );

        Player unmutedByPlayer = Bukkit.getPlayer(UUID.fromString(unmutedBy));
        if (unmutedBy.equals("console")) {
            Bukkit.getConsoleSender().sendMessage(unmuteMessage);
        } else {
            if (unmutedByPlayer != null) {
                unmutedByPlayer.sendMessage(unmuteMessage);
            }
        }
        if (target.isOnline()) {
            ((Player) target).sendMessage(ChatColor.translateAlternateColorCodes('&', Objects.requireNonNull(KBan.getInstance().getConfig().getString("player-unmute-message"))
                    .replaceAll("<player>", unmutedByPlayer != null ? unmutedByPlayer.getName() : "console")
            ));
        }
    }
    public static void unmute(OfflinePlayer target) {
        try {
            MuteUtils.deleteMute(target.getUniqueId().toString());
        } catch (IOException e) {
            throw new RuntimeException(e);
        }

        if (target.isOnline()) {
            ((Player) target).sendMessage(ChatColor.translateAlternateColorCodes('&', Objects.requireNonNull(KBan.getInstance().getConfig().getString("player-unmute-message"))
                    .replaceAll("<player>", "временем")
            ));
        }
    }
}

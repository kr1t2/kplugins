package org.example.krit.kban.utils;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.OfflinePlayer;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.example.krit.kban.KBan;
import org.example.krit.kban.model.Ban;
import org.example.krit.kban.model.Mute;
import org.example.krit.kban.model.Warn;

import java.util.Objects;
import java.util.UUID;

public class MessagesUtils {

    public static void sendMessageToEveryone(CommandSender sender, String message, String permission) {
        if (sender instanceof Player p && !p.hasPermission(permission)) {
            p.sendMessage(message);
        }
        Bukkit.getConsoleSender().sendMessage(message);
        for (Player people: Bukkit.getOnlinePlayers()) {
            if (people.hasPermission(permission)) {
                people.sendMessage(message);
            }
        }
    }
    public static void sendMessageToEveryone(String message, String permission) {
        Bukkit.getConsoleSender().sendMessage(message);
        for (Player people: Bukkit.getOnlinePlayers()) {
            if (people.hasPermission(permission)) {
                people.sendMessage(message);
            }
        }
    }

    public static void sendBanMessage(Ban ban) {
        if (ban.getBanExpires() == 0) {
            sendMessageToEveryone(ChatColor.translateAlternateColorCodes('&', Objects.requireNonNull(Objects.requireNonNull(KBan.getInstance().getConfig().getString("player-ban-message"))
                    .replaceAll("<player>", Objects.requireNonNull(Bukkit.getOfflinePlayer(UUID.fromString(ban.getUuid())).getName()))
                    .replaceAll("<reason>", ban.getReason())
            )), "kban.see.ban");
        } else {
            sendMessageToEveryone(ChatColor.translateAlternateColorCodes('&', Objects.requireNonNull(Objects.requireNonNull(KBan.getInstance().getConfig().getString("player-tempban-message"))
                    .replaceAll("<time>", KBanUtils.millisecToTime(ban.getBanExpires() - System.currentTimeMillis()))
                    .replaceAll("<player>", Objects.requireNonNull(Bukkit.getOfflinePlayer(UUID.fromString(ban.getUuid())).getName()))
                    .replaceAll("<reason>", ban.getReason())
            )), "kban.see.ban");
        }
    }
    public static void sendBanMessage(Ban ban, CommandSender senderToSend) {
        if (ban.getBanExpires() == 0) {
            sendMessageToEveryone(senderToSend, ChatColor.translateAlternateColorCodes('&', Objects.requireNonNull(Objects.requireNonNull(KBan.getInstance().getConfig().getString("player-ban-message"))
                    .replaceAll("<player>", Objects.requireNonNull(Bukkit.getOfflinePlayer(UUID.fromString(ban.getUuid())).getName()))
                    .replaceAll("<reason>", ban.getReason())
            )), "kban.see.ban");
        } else {
            sendMessageToEveryone(senderToSend, ChatColor.translateAlternateColorCodes('&', Objects.requireNonNull(Objects.requireNonNull(KBan.getInstance().getConfig().getString("player-tempban-message"))
                    .replaceAll("<time>", KBanUtils.millisecToTime(ban.getBanExpires() - System.currentTimeMillis()))
                    .replaceAll("<player>", Objects.requireNonNull(Bukkit.getOfflinePlayer(UUID.fromString(ban.getUuid())).getName()))
                    .replaceAll("<reason>", ban.getReason())
            )), "kban.see.ban");
        }
    }


    public static void sendUnbanMessage(CommandSender senderToSend, OfflinePlayer playerToUnban) {
        sendMessageToEveryone(senderToSend, ChatColor.translateAlternateColorCodes('&', Objects.requireNonNull(Objects.requireNonNull(KBan.getInstance().getConfig().getString("player-unban-message"))
                .replaceAll("<player>", Objects.requireNonNull(playerToUnban.getName()))
        )), "kban.see.unban");
    }
    public static void sendUnbanMessage(OfflinePlayer playerToUnban) {
        sendMessageToEveryone(ChatColor.translateAlternateColorCodes('&', Objects.requireNonNull(Objects.requireNonNull(KBan.getInstance().getConfig().getString("player-unban-message"))
                .replaceAll("<player>", Objects.requireNonNull(playerToUnban.getName()))
        )), "kban.see.unban");
    }


    public static void sendBanIpMessage(CommandSender senderToSend, Ban ban) {
        if (ban.getIp() != null) {
            sendMessageToEveryone(senderToSend, ChatColor.translateAlternateColorCodes('&', Objects.requireNonNull(Objects.requireNonNull(KBan.getInstance().getConfig().getString(ban.getBanExpires() == 0 ? "ip-ban-message" : "ip-tempban-message"))
                    .replaceAll("<player>", Objects.requireNonNull(Bukkit.getOfflinePlayer(UUID.fromString(ban.getUuid())).getName()))
                    .replaceAll("<address>", ban.getIp())
                    .replaceAll("<reason>", ban.getReason())
                    .replaceAll("<time>", KBanUtils.millisecToTime(ban.getBanExpires() - System.currentTimeMillis()))
            )), "kban.see.banip");
        } else {
            sendMessageToEveryone(senderToSend, ChatColor.translateAlternateColorCodes('&', Objects.requireNonNull(Objects.requireNonNull(KBan.getInstance().getConfig().getString(ban.getBanExpires() == 0 ? "ip-future-ban-message" : "ip-future-tempban-message"))
                    .replaceAll("<player>", Objects.requireNonNull(Bukkit.getOfflinePlayer(UUID.fromString(ban.getUuid())).getName()))
                    .replaceAll("<reason>", ban.getReason())
                    .replaceAll("<time>", KBanUtils.millisecToTime(ban.getBanExpires() - System.currentTimeMillis()))
            )), "kban.see.banip");
        }
    }
    public static void sendBanIpMessage(Ban ban) {
        if (ban.getIp() != null) {
            sendMessageToEveryone(ChatColor.translateAlternateColorCodes('&', Objects.requireNonNull(Objects.requireNonNull(KBan.getInstance().getConfig().getString(ban.getBanExpires() == 0 ? "ip-ban-message" : "ip-tempban-message"))
                    .replaceAll("<player>", Objects.requireNonNull(Bukkit.getOfflinePlayer(UUID.fromString(ban.getUuid())).getName()))
                    .replaceAll("<address>", ban.getIp())
                    .replaceAll("<reason>", ban.getReason())
                    .replaceAll("<time>", KBanUtils.millisecToTime(ban.getBanExpires() - System.currentTimeMillis()))
            )), "kban.see.banip");
        } else {
            sendMessageToEveryone(ChatColor.translateAlternateColorCodes('&', Objects.requireNonNull(Objects.requireNonNull(KBan.getInstance().getConfig().getString(ban.getBanExpires() == 0 ? "ip-future-ban-message" : "ip-future-tempban-message"))
                    .replaceAll("<player>", Objects.requireNonNull(Bukkit.getOfflinePlayer(UUID.fromString(ban.getUuid())).getName()))
                    .replaceAll("<reason>", ban.getReason())
                    .replaceAll("<time>", KBanUtils.millisecToTime(ban.getBanExpires() - System.currentTimeMillis()))
            )), "kban.see.banip");
        }
    }

    public static void sendWarnMessage(Warn warn, OfflinePlayer target, CommandSender sender) {
        sendMessageToEveryone(sender, ChatColor.translateAlternateColorCodes('&', Objects.requireNonNull(Objects.requireNonNull(KBan.getInstance().getConfig().getString("warn-message"))
                .replaceAll("<player>", Objects.requireNonNull(target.getName()))
                .replaceAll("<warnNum>", String.valueOf(warn.getNumber()))
                .replaceAll("<reason>", warn.getContent())
        )), "kban.see.warn");
    }
    public static void sendWarnMessage(Warn warn, OfflinePlayer target) {
        sendMessageToEveryone(ChatColor.translateAlternateColorCodes('&', Objects.requireNonNull(Objects.requireNonNull(KBan.getInstance().getConfig().getString("warn-message"))
                .replaceAll("<player>", Objects.requireNonNull(target.getName()))
                .replaceAll("<warnNum>", String.valueOf(warn.getNumber()))
                .replaceAll("<reason>", warn.getContent())
        )), "kban.see.warn");
    }
}

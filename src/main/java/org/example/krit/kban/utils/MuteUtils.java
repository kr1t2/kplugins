package org.example.krit.kban.utils;

import com.google.gson.Gson;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.example.krit.kban.KBan;
import org.example.krit.kban.model.Mute;
import org.example.krit.kban.model.Scheduler;

import java.io.*;
import java.util.*;

public class MuteUtils {
    private static ArrayList<Mute> mutes = new ArrayList<>();

    public static Mute mute(String playerUUID, String bannedBy, String reason, long expiresAt) {

        Mute mute = new Mute(playerUUID, bannedBy, reason, expiresAt);
        mutes.add(mute);
        try {
            updateMute(playerUUID, mute);
        } catch (IOException e) {
            Bukkit.getConsoleSender().sendMessage(ChatColor.translateAlternateColorCodes('&', Objects.requireNonNull(KBan.getInstance().getConfig().getString("error-message"))));
            throw new RuntimeException(e);
        }
        if (expiresAt != 0) {
            Scheduler.runLaterAsync(() -> {
                KBanUtils.unmute(Bukkit.getOfflinePlayer(UUID.fromString(playerUUID)));
                System.out.println("realyy");
            }, expiresAt);
        }

        return mute;
    }

    public static void deleteMute(String playerUUID) throws IOException {
        for (Mute mute : mutes) {
            if (mute.getUuid().equals(playerUUID)) {
                mutes.remove(mute);
                saveMutesList();
                break;
            }
        }

    }

    public static Mute findMute(String playerUUID) {
        for (Mute mute : mutes) {
            if (mute.getUuid().equals(playerUUID)) {
                return mute;
            }
        }
        return null;
    }

    public static Mute updateMute(String playerUUID, Mute muteToUpdate) throws IOException {
        for (Mute mute : mutes) {
            if (mute.getUuid().equals(playerUUID)) {

                mute.setMutedBy(muteToUpdate.getMutedBy());
                mute.setExpiresAt(muteToUpdate.getExpiresAt());
                mute.setUuid(muteToUpdate.getUuid());
                mute.setReason(muteToUpdate.getReason());

                saveMutesList();
                break;
            }
        }
        return null;
    }

    public static List<Mute> getMutesList() {
        return mutes;
    }

    public static void loadMutesList() throws IOException {

        Gson gson = new Gson();
        File file = new File(KBan.getInstance().getDataFolder().getAbsolutePath() + "/mutes.json");
        if (file.exists()) {
            Reader reader = new FileReader(file);
            Mute[] n = gson.fromJson(reader, Mute[].class);
            mutes = new ArrayList<>(Arrays.asList(n));
            startTimeCounting();
        }

    }

    public static void saveMutesList() throws IOException {

        Gson gson = new Gson();
        File file = new File(KBan.getInstance().getDataFolder().getAbsolutePath() + "/mutes.json");
        file.getParentFile().mkdir();
        file.createNewFile();
        Writer writer = new FileWriter(file, false);
        gson.toJson(mutes, writer);
        writer.flush();
        writer.close();

    }

    private static void startTimeCounting() {
        for (int i = 0; i < mutes.size(); i++) {
            Mute mute = mutes.get(i);
            if (mute.getExpiresAt() == 0) {
                continue;
            }
            if (mute.getExpiresAt() > System.currentTimeMillis()) {
                Scheduler.runLaterAsync(() -> {
                    KBanUtils.unmute(Bukkit.getOfflinePlayer(UUID.fromString(mute.getUuid())));
                }, mute.getExpiresAt() - System.currentTimeMillis());
            } else {
                try {
                    deleteMute(mute.getUuid());
                } catch (IOException e) {
                    throw new RuntimeException(e);
                }
                i--;
            }
        }
    }
}

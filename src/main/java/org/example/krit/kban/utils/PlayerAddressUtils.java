package org.example.krit.kban.utils;

import com.google.gson.Gson;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.example.krit.kban.KBan;
import org.example.krit.kban.model.Ban;
import org.example.krit.kban.model.PlayerAddress;

import java.io.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;

public class PlayerAddressUtils {
    private static ArrayList<PlayerAddress> addresses = new ArrayList<>();

    public static PlayerAddress addAddress(String playerUUID, String address) {

        PlayerAddress playerAddress = new PlayerAddress(playerUUID, address);
        addresses.add(playerAddress);
        try {
            updateAddress(playerUUID, playerAddress);
        } catch (IOException e) {
            Bukkit.getConsoleSender().sendMessage(ChatColor.translateAlternateColorCodes('&', Objects.requireNonNull(KBan.getInstance().getConfig().getString("error-message"))));
            throw new RuntimeException(e);
        }

        return playerAddress;
    }

    public static void deleteAddress(String playerUUID) throws IOException {
        for (PlayerAddress playerAddress : addresses) {
            if (playerAddress.getPlayerUUID().equals(playerUUID)) {
                addresses.remove(playerAddress);
                updateAddress(playerUUID, playerAddress);
                break;
            }
        }

    }

    public static PlayerAddress findAddress(String playerUUID) {
        for (PlayerAddress address : addresses) {
            if (address.getPlayerUUID().equals(playerUUID)) {
                return address;
            }
        }
        return null;
    }

    public static Ban updateAddress(String playerUUID, PlayerAddress addressToUpdate) throws IOException {
        for (PlayerAddress address : addresses) {
            if (address.getPlayerUUID().equals(playerUUID)) {

                address.setAddress(address.getAddress());
                address.setPlayerUUID(addressToUpdate.getPlayerUUID());

                saveBanList();
                break;
            }
        }
        return null;
    }

    public static List<PlayerAddress> getAddresses() {
        return addresses;
    }

    public static void loadAddresses() throws IOException {

        Gson gson = new Gson();
        File file = new File(KBan.getInstance().getDataFolder().getAbsolutePath() + "/addresses.json");
        if (file.exists()) {
            Reader reader = new FileReader(file);
            PlayerAddress[] n = gson.fromJson(reader, PlayerAddress[].class);
            addresses = new ArrayList<>(Arrays.asList(n));
        }

    }

    public static void saveBanList() throws IOException {

        Gson gson = new Gson();
        File file = new File(KBan.getInstance().getDataFolder().getAbsolutePath() + "/addresses.json");
        file.getParentFile().mkdir();
        file.createNewFile();
        Writer writer = new FileWriter(file, false);
        gson.toJson(addresses, writer);
        writer.flush();
        writer.close();

    }
}

package org.example.krit.kban.utils;


import com.google.gson.Gson;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.example.krit.kban.KBan;
import org.example.krit.kban.model.Ban;
import org.example.krit.kban.model.Warn;
import org.example.krit.kban.model.Warns;

import java.io.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;

public class WarnsUtils {

    private static ArrayList<Warns> playerWarns = new ArrayList<>();

    public static Warns addWarns(String playerUUID, ArrayList<Warn>warns) {

        Warns warn = new Warns(playerUUID, warns);
        playerWarns.add(warn);
        try {
            updateWarns(playerUUID, warn);
        } catch (IOException e) {
            Bukkit.getConsoleSender().sendMessage(ChatColor.translateAlternateColorCodes('&', Objects.requireNonNull(KBan.getInstance().getConfig().getString("error-message"))));
            throw new RuntimeException(e);
        }

        return warn;
    }

    public static void deleteWarns(String playerUUID) throws IOException {
        for (Warns warns : playerWarns) {
            if (warns.getPlayerUUID().equals(playerUUID)) {
                playerWarns.remove(warns);
                saveWarnsList();
                break;
            }
        }

    }

    public static Warns findWarns(String playerUUID) {
        for (Warns warns : playerWarns) {
            if (warns.getPlayerUUID().equals(playerUUID)) {
                return warns;
            }
        }
        return null;
    }

    public static Ban updateWarns(String playerUUID, Warns warnsToUpdate) throws IOException {
        for (Warns warns : playerWarns) {
            if (warns.getPlayerUUID().equals(playerUUID)) {

                warns.setPlayerUUID(warnsToUpdate.getPlayerUUID());
                warns.setWarns(warnsToUpdate.getWarns());

                saveWarnsList();
                break;
            }
        }
        return null;
    }

    public static List<Warns> getWarnsList() {
        return playerWarns;
    }

    public static void loadWarnsList() throws IOException {

        Gson gson = new Gson();
        File file = new File(KBan.getInstance().getDataFolder().getAbsolutePath() + "/warnslist.json");
        if (file.exists()) {
            Reader reader = new FileReader(file);
            Warns[] n = gson.fromJson(reader, Warns[].class);
            playerWarns = new ArrayList<>(Arrays.asList(n));
        }

    }

    public static void saveWarnsList() throws IOException {

        Gson gson = new Gson();
        File file = new File(KBan.getInstance().getDataFolder().getAbsolutePath() + "/warnslist.json");
        file.getParentFile().mkdir();
        file.createNewFile();
        Writer writer = new FileWriter(file, false);
        gson.toJson(playerWarns, writer);
        writer.flush();
        writer.close();

    }

}